/**
* Created by J.Espada on 15/05/2016.
*/
http_request = false;
xmlhttp = new XMLHttpRequest();

var xml_tree;
var carrinho_Compras = [];
var conta = 0;


function setCookie(email,password,nome,curso,numero)
{

  document.cookie = "user=" + email + " " + password +" " + nome +" " + curso +" " + numero;
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function checkCookie() {
  var user = getCookie("user");
  if (user != "") {
    //alert("Welcome again " + user);
  } else {
    //user = prompt("Please enter your name:", "");
    if (user != "" && user != null) {
      //setCookie("user", user, 365);
    }
  }
}

function criarProduto(tipo, preco, disponibilidade, nome) {
  this.tipo_prod = tipo;
  this.preco_prod = preco;
  this.disponibilidade_prod = disponibilidade;
  this.id_prod = nome;
  this.quantidade = 0 ;
}



function getUserInfo() {
  var str = getCookie("user");
  var strArray = str.split(" ");
  //console.log( strArray);

  document.getElementById("nome").innerHTML= "Nome : "+strArray[2]+ " " + strArray[3];
  document.getElementById("numero").innerHTML = "Número : "+strArray[6];
  document.getElementById("curso").innerHTML = "Curso : " + strArray[4] + " " +strArray[5];

}

function loadXML(xmlFile) {

  http_request = false;
  if (window.XMLHttpRequest) { // Mozilla, Safari,...
    http_request = new window.XMLHttpRequest();

  } else if (window.ActiveXObject) { // IE
    try {
      http_request = new ActiveXObject("Microsoft.XMLHTTP");
    } catch (e) {
      try {
        http_request = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (e) {
      }
    }
  }

  if (!http_request) {
    alert('Giving up :( Cannot create an XMLHTTP instance');
    return false;
  }

  http_request.open("GET", xmlFile, false);

  http_request.send(null);
  xml_tree = http_request.responseXML;
}


function validate() {

  loadXML("utilizadores.xml");
  var x = xml_tree.getElementsByTagName("utilizador");

  var inputMail = document.getElementById("inputMail").value;
  var inputPassword = document.getElementById("inputPassword").value;
  var alerta = document.getElementById("loginFail");

  var bool = false;


  for (i = 0; i < x.length; i++) {

    if (inputMail == x[i].getElementsByTagName("email")[0].childNodes[0].nodeValue && inputPassword == x[i].getElementsByTagName("password")[0].childNodes[0].nodeValue) {

      setCookie(x[i].getElementsByTagName("email")[0].childNodes[0].nodeValue,x[i].getElementsByTagName("password")[0].childNodes[0].nodeValue,x[i].getElementsByTagName("nome")[0].childNodes[0].nodeValue,
      x[i].getElementsByTagName("curso")[0].childNodes[0].nodeValue, x[i].getElementsByTagName("numero")[0].childNodes[0].nodeValue);
      bool = true;
      break;

    }


  }

  if (bool == false) {
    alerta.innerHTML = '<div class="alert alert-danger"><strong>Erro!</strong> Dados de autenticação estão errados</div> </div>';

  }


  return bool;

}

function xmlFaltas() {
  loadXML("utilizadores.xml");
  var x = xml_tree.getElementsByTagName("utilizador");
  var table='<table class="table"> <thead> <tr> <th>Disciplina</th> <th>Data da última falta</th> <th>Total de faltas</th>  </thead> <tbody> ';
  var user_email = getCookie("user");

  for (var i = 0; i <x.length; i++) {

    if(user_email.split(" ")[0] == x[i].getElementsByTagName("email")[0].childNodes[0].nodeValue)
    {

      for(var j = 0 ; j < x[i].getElementsByTagName("faltaDisciplina").length ; j++){

        var disciplina = x[i].getElementsByTagName("faltaDisciplina")[j].childNodes[0].nodeValue;

        var data = x[i].getElementsByTagName("faltaDia")[j].childNodes[0].nodeValue;

        var totalFaltas = x[i].getElementsByTagName("faltaTotal")[j].childNodes[0].nodeValue;

        table = table + '<tr> <td>' + disciplina + '</td> <td>' + data + '</td> <td> '+ totalFaltas + '  </td> </tr> ';

      }


    }

  }
  table += "</tbody></table>"
  document.getElementById("faltaTabela").innerHTML = table;
}

function xmlMovimentos() {

  loadXML("utilizadores.xml");
  var x = xml_tree.getElementsByTagName("utilizador");
  var table='<table class="table"> <thead> <tr> <th>Fonte</th> <th>Data</th> <th>Quantia</th></tr> </thead> <tbody> ';
  var user_email = getCookie("user");

  for (var i = 0; i <x.length; i++) {

    if(user_email.split(" ")[0] == x[i].getElementsByTagName("email")[0].childNodes[0].nodeValue)
    {

      for(var j = 0 ; j < x[i].getElementsByTagName("fonte").length ; j++)
      {
        var fonte = x[i].getElementsByTagName("fonte")[j].childNodes[0].nodeValue ;
        var data =  x[i].getElementsByTagName("data")[j].childNodes[0].nodeValue ;
        var quantia =x[i].getElementsByTagName("quantia")[j].childNodes[0].nodeValue ;

        table = table + '<tr> <td>' + fonte + '</td> <td>' + data+ '</td> <td> ' + quantia + "&euro;" + '</td> </tr> ';
      }

    }

  }
  table = table + '</tbody> </table>';
  document.getElementById("movimentosTabela").innerHTML = table;
}

function xmlSaldo() {

  loadXML("utilizadores.xml");
  var x = xml_tree.getElementsByTagName("utilizador");
  var user_email = getCookie("user");

  for (var i = 0; i <x.length; i++) {

    if(user_email.split(" ")[0] == x[i].getElementsByTagName("email")[0].childNodes[0].nodeValue)
    {
      document.getElementById("saldo-disponivel").innerHTML = "Possui " + x[i].getElementsByTagName("saldo")[0].childNodes[0].nodeValue + "&euro;";
    }

  }

}

function xmlNoticias() {
  loadXML("noticias.xml");
  var x = xml_tree.getElementsByTagName("noticia");

  for(var i= 0 ; i <x.length ; i++) {
    var titulo = x[i].getElementsByTagName("titulo")[0].childNodes[0].nodeValue;
    var imgScr = x[i].getElementsByTagName("img")[0].childNodes[0].nodeValue;
    var descricao = x[i].getElementsByTagName("descricao")[0].childNodes[0].nodeValue;
    document.getElementById("noticiasDiv").innerHTML +='<div class="col-md-4"> <article> <h3>'+titulo+'</h3> <img class="img-responsive" src="'+imgScr+'" alt="'+titulo+'"> <p class="p1"> '+descricao+'</p> <a class="lerMais" href="#"> Ver mais ...</a></article></div>';
  }

}

function xmlEventos() {
  loadXML("eventos.xml");
  var x = xml_tree.getElementsByTagName("evento");

  for(var i= 0 ; i <x.length ; i++) {
    var titulo = x[i].getElementsByTagName("titulo")[0].childNodes[0].nodeValue;
    var imgScr = x[i].getElementsByTagName("img")[0].childNodes[0].nodeValue;
    var descricao = x[i].getElementsByTagName("descricao")[0].childNodes[0].nodeValue;
    document.getElementById("eventosDiv").innerHTML +='<div class="col-md-4"> <article> <h3>'+titulo+'</h3> <img class="img-responsive" src="'+imgScr+'" alt="'+titulo+'"> <p class="p1">'+descricao+'</p> <a class="lerMais" href="#"> Ver mais ...</a></article></div>';
  }

}


function loadFooter() {

  var node = document.getElementById("footer");

  node.innerHTML =
  `
  <div class="container">
  <div class="row">
  <div class="col-md-12">
  <div class="col-md-3">
  <div class="col-md-12">
  <img src="http://www.quidgest.pt/images/QuidPartner_31/ipbeja_vertical.png"
  class="img-responsive"
  alt="Logotipo IPBEJA">
  <div class="col-md-12">
  <div id="social-icons">
  <a href="https://twitter.com/"> <img src="https://cdn1.iconfinder.com/data/icons/logotypes/32/twitter-128.png" width="48" height="48" alt="Twitter" /></a>
  <a href="http://www.linkedin.com/"> <img src="http://dambrosio-correll.info/images/linkedInIcon.png" width="48" height="48" alt="LinkedIn" /></a>
  <a href="http://www.facebook.com/"> <img src="https://www.facebookbrand.com/img/fb-art.jpg" width="48" height="48" alt="Facebook" /></a>
  </div>
  </div>
  </div>
  </div>

  <div class="col-md-6">
  <div id="map-responsive">
  <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12573.749211674409!2d-7.8744179!3d38.0135835!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x492c0dec60aa1414!2sInstituto+polit%C3%A9cnico+de+Beja!5e0!3m2!1spt-PT!2spt!4v1463328159636"
  ></iframe>
  </div>
  </div>
  <div class="col-md-3">
  <h4>Contactos</h4>
  <p>Telefone : +351 284 315 000</p>
  <p>Email : geral@ipbeja.pt</p>
  <p>Fax: +351 284 314 401</p>
  <h4>Morada</h4>
  <p>Instituto Politécnico de Beja
  Rua Pedro Soares
  Campus do Instituto Politécnico de Beja
  Apartado 6155
  7800-295 Beja </p>
  </div>
  <div class="col-md-12">
 <hr class="dash"/>
  <div class="col-md-2">©2016 - Instituto Politécnico de Beja</div>
  <div class="col-md-2"><a href="cubo.html">Loja Cubo</a></div>
 <div class="col-md-2"><a href="#">Contactos</a></div>
 <div class="col-md-2"><a href="#">Mapa do Site</a></div>
 <div class="col-md-2"><a href="#">Termos de utilização</a></div>
  <div class="col-md-2"><button class="btn btn-default" onclick="scrollUp();">Voltar ao topo &uarr;</button></div>
 </div>

  </div>
  </div>

  </div>

  </div>`;
}

function scrollUp() {

    window.scrollTo(0, 0);
}

function mostrarEsconder(mostrar, esconder) {
  $(mostrar).hide();
  $(esconder).show();
}

function mostrarGerirCartao() {

  document.getElementById("link-gerir").className = "active";
  $("#info-inicial").hide();
  $("#mostrarCartao").show();
}

function carregarFinalizar() {
  $("#finalizar-sucesso").show();

}

function mostrarInfoPagamento() {
  $("#anterior").hide();
  var element = document.getElementById("usr").value;
  if (isNaN(element) == false && element.length > 0) {

    document.getElementById("valor").innerHTML = "Quantia - " + document.getElementById("usr").value + "&euro;";
    $("#info-pagamento").show();
    $("#carregar-cartao-alerta").hide();

  }
  else {

    $("#carregar-cartao-alerta").show();
  }

}


function mostrarMaterialEscolar() {

  document.getElementById("material-escolar").className = "active";
  $("#carrinho-compras").hide();
  $("#carousel-cubo").show();
  $("#novo-produto").hide();
  $("#info-pagamento").hide();
  $("#produtos-escolares").show();
}

function adicionarCarrinho(str) {

  var x = str;
  console.log(str);
  var prodComprado;
  $("#alerta-adicionado").hide();
  for (i = 0; i < produtos.length; i++) {

    if (produtos[i].id_prod == x) {
      prodComprado = produtos[i];
      prodComprado.quantidade++;
      document.getElementById("alerta-adicionado").innerHTML = '<div class="alert alert-success fade in"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong>Successo!</strong> O produto foi adicionado ao carrinho</div>';
      $("#alerta-adicionado").show();
      break;
    }

  }
  if (prodComprado.quantidade < 2) {
    carrinho_Compras.push(prodComprado);
  }

}

function mostrarCarrinho() {
  document.getElementById("material-escolar").className = " ";
  $("#carrinho-compras").show();
  $("#produtos-escolares").hide();
  $("#info-pagamento").hide();
  $("#carousel-cubo").hide();
  $("#novo-produto").hide();
  $("#nao-itens-carrinho").hide();
  calcularConta();
}

function check() {
  var mul = document.getElementById("multibanco").checked;
  var transf = document.getElementById("transferencia").checked;
  var levantarLoja = document.getElementById("levantar-loja").checked;
  var info = document.getElementById("info-pagamento");
  var quantia = "Quantia ";
  var euros = "&euro;";

  if (mul == true && conta > 0) {

    info.innerHTML+= '<div class="alert alert-info"><strong>Dados de Pagamento</strong> <p>Ref YYYYY</p> <p>Entidade XXX </p> <p>' + quantia + conta + euros + '</p> </div>';

    document.getElementById("inputConfirmar").disabled = true;
    $("#info-pagamento").show();
  }
  else if (transf == true && conta > 0) {
    info.innerHTML = '<div class="alert alert-info"><strong>Dados de Pagamento</strong> <p>NIB: YYYYY</p> <p>' + quantia + conta + euros + '</p> </div>';
    document.getElementById("inputConfirmar").disabled = true;
    $("#info-pagamento").show();
  }
  else if (levantarLoja == true && conta > 0) {
    info.innerHTML = '<div class="alert alert-info"><strong>Dados de Pagamento</strong> Pode ir á loja levantar a encomenda </div>';
    document.getElementById("inputConfirmar").disabled = true;
    $("#info-pagamento").show();
  }
  else
  {
    document.getElementById("nao-itens-carrinho").innerHTML= '<div class="alert alert-danger"> <strong>Erro!</strong> Não têm produtos no carrinho</div>';
  }


}

function calcularConta() {
  conta = 0 ;
  document.getElementById("lista-carrinho").innerHTML = "";

  var x = '<table class="table"> <thead> <tr> <th>Nome Produto</th> <th>Preço por Unidade</th> <th>Quantidade</th> <th>Preço</th></tr> </thead> <tbody> ';

  for (var i = 0; i < carrinho_Compras.length; i++)
  {

    var n = obterQuantidade(carrinho_Compras[i].id_prod);
    var prod = carrinho_Compras[i];
    var quantidade = '<span><input type="text" ' + ' value="'+ n +'" ' +' size="4" > <button type="button" class="btn btn-default" onclick="incrementarProd('+i+')" > + </button> <button type="button" class="btn btn-default" onclick="decrementarProd('+i+')">-</button></span>';


    x = x + '<tr> <td>' + prod.id_prod + '</td> <td>' + prod.preco_prod + '</td> <td> ' + quantidade + '</td> <td> ' + n*prod.preco_prod + "&euro;" + '  </td>  </tr>';

    conta = conta + parseInt(n*prod.preco_prod);
  }

  x = x + '<tr> <td>' + conta + "&euro;"+ '</td> </tr> </tbody> </table>' ;
  document.getElementById("lista-carrinho").innerHTML = x;

}

function incrementarProd(prodIndex){
  carrinho_Compras[prodIndex].quantidade ++ ;
  calcularConta();
}

function decrementarProd(prodIndex){
  if(carrinho_Compras[prodIndex].quantidade > 0) {

    carrinho_Compras[prodIndex].quantidade -- ;
    calcularConta();
  }


}

function obterQuantidade(id) {
  var counter = 0;

  for (var j = 0; j < carrinho_Compras.length; j++)
  {
    if(carrinho_Compras[j].id_prod == id){
      counter = carrinho_Compras[j].quantidade;
      break;
    }
  }

  return counter;
}


function removerItem() {
  conta = 0 ;
  var x = document.getElementById("mySelect");
  var prod =  x.options[x.selectedIndex].value;
  x.remove(x.selectedIndex);
  var novoCarrinho = [];
  var removerUm = 0 ;
  for (i = 0; i < carrinho_Compras.length; i++)
  {
    if(carrinho_Compras[i].id_prod == prod && removerUm == 0){
      removerUm = 1;
    }
    else
    {
      novoCarrinho.push(carrinho_Compras[i]);

    }

  }

  carrinho_Compras = novoCarrinho;
  calcularConta();
}

function carregarProdutos2() {
  loadXML("produtos.xml");
  var x = xml_tree.getElementsByTagName("produto");
  produtos = [];
  var materialEscolar = document.getElementById("materialEscolar");
  for (i = 0; i < x.length; i++)
  {
    var divisao = x.length/2 -1;
    nome = x[i].getElementsByTagName("nome")[0].childNodes[0].nodeValue;
    //console.log(nome);
    var tipo = x[i].getElementsByTagName("tipo")[0].childNodes[0].nodeValue;
    var preco = x[i].getElementsByTagName("preco")[0].childNodes[0].nodeValue;
    var imgScr = x[i].getElementsByTagName("imagem")[0].childNodes[0].nodeValue;
    var disponibilidade = x[i].getElementsByTagName("disponibilidade")[0].childNodes[0].nodeValue;
    var temp_prod = new criarProduto(tipo, preco, disponibilidade, nome);
    produtos[i] = temp_prod;
    if(disponibilidade == "true") {

      materialEscolar.innerHTML += '<div class="col-md-3"> <img src="'+imgScr+'" class="img-responsive" alt="'+nome+'" > <p>'+nome+'</p> <p>'+preco+'&euro;</p> <span>Disponibilidade <img src="http://www.bikefernie.ca/sites/default/files/images/difficulty-green-circle-40.png" class="disponibilidade" alt="disponivel"> </span> <br> <button type="button" class="btn btn-success active" onclick=adicionarCarrinho("'+nome+'")>Adicionar Carrinho</button> </div>';
    }

    else
    {
      var nome = x[i].getElementsByTagName("nome")[0].childNodes[0].nodeValue;
      materialEscolar.innerHTML += '<div class="col-md-3"> <img src="'+imgScr+'" class="img-responsive"  alt="'+nome+' " > <p>'+nome+'</p> <p>'+preco+'&euro;</p> <span>Disponibilidade <img src="https://wiki.cam.ac.uk/wiki/university-map/img_auth.php/8/87/Circle2.png" class="disponibilidade"> </span> </span> <br> <button type="button" class="btn btn-danger disabled" >Adicionar Carrinho</button> </div> ';

    }

  }
}
